import { Resource } from './Resource';
import { UserOwned } from './UserOwned';

export interface UserOwnedResource extends UserOwned, Resource {
}
