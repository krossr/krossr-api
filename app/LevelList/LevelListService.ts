import { LevelListQuery } from './LevelListQuery';
import { inject } from 'inversify';
import { LevelListSortByOptions } from './LevelListSortByOptions';
import { SizeOptions } from '../Size/SizeOptions';
import { SizeFormatter } from '../Size/SizeFormatter';
import { SortDirectionOptions } from '../SortDirection/SortDirectionOptions';
import { LevelListRepository } from './LevelListRepository';
import { User } from '../models/UserModel';
import { provide } from 'inversify-binding-decorators';
import { LevelListFilterSelectOptionsViewModel } from './LevelListFilterSelectOptionsViewModel';

@provide(LevelListService)
export class LevelListService {
    constructor(
        @inject(SizeFormatter) private sizeFormatter: SizeFormatter,
        @inject(LevelListRepository) private levelListRepository: LevelListRepository
    ) {
    }

    public getOptions(): LevelListFilterSelectOptionsViewModel {
        let sortByOptions = {
            'Created Date': LevelListSortByOptions.CreatedDate,
            Name: LevelListSortByOptions.Name,
            Ratings: LevelListSortByOptions.Ratings
        };

        let sizeOptions: { [key: string]: number } = {
            All: null
        };

        SizeOptions.Options.forEach(size => {
            sizeOptions[this.sizeFormatter.formatSize(size)] = size;
        });

        let sortDirectionOptions = {
            Ascending: SortDirectionOptions.ASC,
            Descending: SortDirectionOptions.DESC
        };

        return { sortByOptions, sizeOptions, sortDirectionOptions };
    }

    public getList(query: LevelListQuery, user: User) {
        return this.levelListRepository.getList(query, user);
    }
}
