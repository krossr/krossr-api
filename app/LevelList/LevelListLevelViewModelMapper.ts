import { ViewModelMapper } from '../ViewModel/ViewModelMapper';
import { Level } from '../models/LevelModel';
import { inject } from 'inversify';
import { SizeFormatter } from '../Size/SizeFormatter';
import { UserViewModelMapper } from '../Users/UserViewModelMapper';
import { User } from '../models/UserModel';
import { AuthorizationService } from '../Authorization/AuthorizationService';
import { provide } from 'inversify-binding-decorators';
import { LevelListLevelViewModel } from './LevelListLevelViewModel';

@provide(LevelListLevelViewModelMapper)
export class LevelListLevelViewModelMapper implements ViewModelMapper<Level, LevelListLevelViewModel> {
    constructor(
        @inject(AuthorizationService) private authorizationService: AuthorizationService,
        @inject(SizeFormatter) private sizeFormatter: SizeFormatter,
        @inject(UserViewModelMapper) private userMapper: UserViewModelMapper
    ) {
    }

    toViewModel(model: Level, currentUser: User): LevelListLevelViewModel {
        let dataValues = (model as any).dataValues; // TODO

        let editable = this.isEditable(model, currentUser);

        return {
            completed: dataValues.completed,
            editable,
            id: model.id,
            layout: model.layout,
            name: model.name,
            prettySize: this.sizeFormatter.formatSize(model.size),
            size: model.size,
            avgRating: dataValues ? dataValues.avgRating : 'n/a',
            createdAt: model.createdAt ? model.createdAt.toString() : null,
            user: this.userMapper.toViewModel(model.user)
        };
    }

    isEditable(model: Level, currentUser: User): boolean {
        return this.authorizationService.canUpdate(model, currentUser);
    }
}
