import { LevelViewModel } from '../Levels/LevelViewModel';
import { UserViewModel } from '../Users/UserViewModel';

export interface LevelListLevelViewModel extends LevelViewModel {
    avgRating: string;
    completed: boolean;
    createdAt: string;
    user: UserViewModel;
    /** Formatted for display */
    prettySize: string;
    editable: boolean;
}
