export interface ResetPasswordBodyViewModel {
    newPassword: string;
    verifyPassword: string;
}
