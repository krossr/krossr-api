import { EnvironmentConfiguration } from '../../config/environmentConfig';
import { User } from '../models/UserModel';
import { UserViewModelMapper } from '../Users/UserViewModelMapper';
import { IEnvironmentConfiguration } from '../../config/env/IEnvironmentConfiguration';
import { inject } from 'inversify';
import { ResetPasswordRequest } from './ResetPasswordRequest';
import { PasswordService } from '../Password/PasswordService';
import { IMailerService } from '../Mailer/IMailerService';
import { MailerSymbols } from '../Mailer/MailerSymbols';
import { UserService } from '../Users/UserService';
import { provide } from 'inversify-binding-decorators';
import { Body, Get, Path, Post, Request, Res, Route, TsoaResponse } from 'tsoa';
import { ErrorResponse } from '../Error/ErrorResponse';
import { ErrorMessageService } from '../Error/ErrorMessageService';
import { HtmlRenderer } from '../HtmlRenderer/HtmlRenderer';
import { UserViewModel } from '../Users/UserViewModel';
import { ResetPasswordBodyViewModel } from './ResetPasswordBodyViewModel';

@provide(ResetPasswordController)
@Route('auth/reset')
export class ResetPasswordController {
    private config: IEnvironmentConfiguration;

    constructor(
        @inject(EnvironmentConfiguration) private environmentConfiguration: EnvironmentConfiguration,
        @inject(ErrorMessageService) private errorMessageService: ErrorMessageService,
        @inject(HtmlRenderer) private htmlRenderer: HtmlRenderer,
        @inject(MailerSymbols.MailerService) private mailerService: IMailerService,
        @inject(PasswordService) private passwordService: PasswordService,
        @inject(UserViewModelMapper) private userMapper: UserViewModelMapper,
        @inject(UserService) private userService: UserService
    ) {
        this.config = this.environmentConfiguration.getConfiguration();
    }

    /**
     * Reset password GET from email token
     */
    @Get('{token}')
    public async validateResetToken(
        @Path() token: string,
        @Request() req: ResetPasswordRequest,
        @Res() serverError: TsoaResponse<500, ErrorResponse>
    ) {
        try {
            let user = await this.userService.getUserByToken(token);
            return { valid: !!user };
        } catch (err) {
            serverError(500, this.errorMessageService.getErrorResponse(err));
        }
    }

    @Post('{token}')
    public async reset(
        @Path() token: string,
        @Request() req: ResetPasswordRequest,
        @Body() passwordDetails: ResetPasswordBodyViewModel,
        @Res() clientError: TsoaResponse<400, ErrorResponse>,
        @Res() serverError: TsoaResponse<500, ErrorResponse>
    ): Promise<UserViewModel> {
        try {
            let user = await this.userService.getUserByToken(token);

            if (!user) {
                return clientError(400, { message: 'Password reset token is invalid or has expired' });
            }

            if (passwordDetails.newPassword !== passwordDetails.verifyPassword) {
                return clientError(400, { message: 'Passwords do not match' });
            }

            await this.resetPassword(req, user);

            let emailTemplate = await this.renderEmailTemplate(user);

            await this.sendEmail(user.email, emailTemplate);

            return new Promise((resolve, reject) => {
                req.login(user, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        // Return authenticated user
                        let result = this.userMapper.toViewModel(user);
                        resolve(result);
                    }
                });
            });
        } catch (err) {
            serverError(500, this.errorMessageService.getErrorResponse(err));
        }
    }

    private async resetPassword(req: ResetPasswordRequest, user: User) {
        let passwordDetails = req.body;

        user.resetPasswordToken = null;
        user.resetPasswordExpires = null;

        this.passwordService.setPassword(user, passwordDetails.newPassword);

        await this.userService.saveUser(user);
    }

    private async renderEmailTemplate(user: User) {
        return new Promise<string>((resolve, reject) => {
            try {
                let emailHTML = this.htmlRenderer.render('app/views/templates/reset-password-confirm-email.server.view.html',
                    {
                        name: user.username,
                        appName: this.config.app.title
                    }
                );

                resolve(emailHTML);
            }
            catch (err) {
                reject(err);
            }
        });
    }

    private async sendEmail(to: string, emailHTML: string) {
        let mailOptions = {
            to,
            from: this.config.mailer.from,
            subject: 'Your password has been changed',
            html: emailHTML
        };

        await this.mailerService.send(mailOptions);
    }
}
