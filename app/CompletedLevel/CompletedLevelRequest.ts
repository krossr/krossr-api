import { KrossrRequest } from '../KrossrRequest/KrossrRequest';
import { Level } from '../models/LevelModel';

export interface CompletedLevelRequest extends KrossrRequest {
    level: Level;
}
