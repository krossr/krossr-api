import { inject } from 'inversify';
import { provide } from 'inversify-binding-decorators';
import { Path, Post, Request, Res, Route, Security, TsoaResponse } from 'tsoa';
import { Securities } from '../Authorization/Securities';
import { ErrorMessageService } from '../Error/ErrorMessageService';
import { ErrorResponse } from '../Error/ErrorResponse';
import { CompletedLevelRequest } from './CompletedLevelRequest';
import { CompletedLevelService } from './CompletedLevelService';

@provide(CompletedLevelController)
@Route('levels')
export class CompletedLevelController {
    constructor(
        @inject(ErrorMessageService) private errorMessageService: ErrorMessageService,
        @inject(CompletedLevelService) private completedLevelService: CompletedLevelService
    ) {
    }

    @Post('{levelId}/complete')
    @Security(Securities.RequiresLogin)
    public async completeLevel(
        @Path() levelId: string,
        @Request() req: CompletedLevelRequest,
        @Res() serverError: TsoaResponse<500, ErrorResponse>
    ) {
        try {
            return await this.completedLevelService.complete(req.level, req.user);
        } catch (err) {
            serverError(500, this.errorMessageService.getErrorResponse(err));
        }
    }
}
