/* istanbul ignore file */

import { inject } from 'inversify';
import { provide } from 'inversify-binding-decorators';
import { KrossrLogger } from '../Logger/KrossrLogger';
import { KrossrLoggerProvider } from '../Logger/KrossrLoggerProvider';
import { LoggerSymbols } from '../Logger/LoggerSymbols';
import { CompletedLevel, CompletedLevelCreationAttributes } from '../models/CompletedLevelModel';

@provide(CompletedLevelRepository)
export class CompletedLevelRepository {
    private logger: KrossrLogger;

    constructor(
        @inject(LoggerSymbols.KrossrLogger) private loggerProvider: KrossrLoggerProvider,
    ) {
        this.logger = this.loggerProvider.getLogger();
    }

    async getCompletedLevel(attributes: CompletedLevelCreationAttributes) {
        let where = {
            userId: attributes.userId,
            levelId: attributes.levelId
        };

        return await CompletedLevel.findOne({ where });
    }

    async upsert(attributes: CompletedLevelCreationAttributes) {
        let where = {
            userId: attributes.userId,
            levelId: attributes.levelId
        };

        this.logger.info('Attempting...');

        return new Promise<void>((resolve, reject) => {
            CompletedLevel.findOrCreate({
                where,
                defaults: attributes
            }).then(() => {
                this.logger.info('success');
                resolve();
            }).catch(err => {
                this.logger.error('failure', err);
                reject();
            });
        });
    }
}
