import { SignUpService } from './SignUpService';
import { SequelizeContainer } from '../../spec/containers/sequelize-container';
import { UserService } from '../Users/UserService';
import { UserViewModelMapper } from '../Users/UserViewModelMapper';
import { PasswordService } from '../Password/PasswordService';
import { UserRepository } from '../Users/UserRepository';
import { CreateUserBodyViewModel } from './CreateUserBodyViewModel';

describe('SignUpService', () => {
    let service: SignUpService;
    let userService: UserService;

    beforeEach(async () => {
        let DIContainer = await new SequelizeContainer().initialize();

        DIContainer.bind(SignUpService).toSelf();
        DIContainer.bind(PasswordService).toSelf();
        DIContainer.bind(UserViewModelMapper).toSelf();
        DIContainer.bind(UserService).toSelf().inSingletonScope();
        DIContainer.bind(UserRepository).toSelf().inSingletonScope();

        service = DIContainer.get(SignUpService);
        userService = DIContainer.get(UserService);
    });

    it('should exist', () => {
        expect(service).toBeTruthy();
    });

    it('should sign up', async () => {
        spyOn(userService, 'saveUser');

        let params: CreateUserBodyViewModel = {
            email: 'fake@fake.com',
            username: 'fake',
            password: 'reallybadpassword'
        };

        await service.signUp(params);
        expect(userService.saveUser).toHaveBeenCalled();
    });
});
