/* istanbul ignore file */
/**
 * crypto is built into node.js, we don't need to test it.
 * but we do need to stub it
 */

import * as crypto from 'crypto';
import { provide } from 'inversify-binding-decorators';

@provide(CryptoService)
export class CryptoService {
    randomBytes(howMany: number): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            crypto.randomBytes(howMany, (err, buffer) => {
                if (err) {
                    reject(err);
                }

                resolve(buffer.toString('hex'));
            });
        });
    }
}
