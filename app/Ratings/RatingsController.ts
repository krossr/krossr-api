import { inject } from 'inversify';
import { UpsertRatingRequest } from './UpsertRatingRequest';
import { RatingsService } from './RatingsService';
import { provide } from 'inversify-binding-decorators';
import { Body, Path, Post, Request, Res, Route, Security, TsoaResponse } from 'tsoa';
import { ErrorResponse } from '../Error/ErrorResponse';
import { ErrorMessageService } from '../Error/ErrorMessageService';
import { Securities } from '../Authorization/Securities';
import { UpsertRatingBodyViewModel } from './UpsertRatingBodyViewModel';

@provide(RatingsController)
@Route('levels')
export class RatingsController {
    constructor(
        @inject(ErrorMessageService) private errorMessageService: ErrorMessageService,
        @inject(RatingsService) private ratingsService: RatingsService
    ) {
    }

    @Post('{levelId}/ratings')
    @Security(Securities.RequiresLogin)
    public async upsertRating(
        @Path() levelId: string,
        @Body() body: UpsertRatingBodyViewModel,
        @Request() req: UpsertRatingRequest,
        @Res() serverError: TsoaResponse<500, ErrorResponse>
    ) {
        try {
            return await this.ratingsService.upsertRating(req.level, req.user, body);
        } catch (err) {
            serverError(500, this.errorMessageService.getErrorResponse(err));
        }
    }
}
