import { Securities } from './Securities';
import { AuthorizationService } from '../Authorization/AuthorizationService';
import { Resources } from './Resources';
import { User } from '../models/UserModel';
import { Resource } from '../models/Resource';
import { UserOwnedResource } from '../models/UserOwnedResource';

export function expressAuthentication(
    request: Express.Request,
    securityName: string,
    scopes?: string[]
): Promise<any> {
    const resolveWithUser = (proceed: boolean, errorMessage: string) => {
        return proceed ? Promise.resolve(request.user) : Promise.reject(new Error(errorMessage));
    };

    if (securityName === Securities.RequiresLogin) {
        return resolveWithUser(request.isAuthenticated(), 'User is not authenticated');
    }

    let authService = new AuthorizationService();
    authService.configure();

    let canProceed = (strategy: (resource: Resource, user: User) => boolean): boolean => {
         // dubious, todo?
         let resourceName = scopes[0] as Resources;
         let resource = (request as any)[resourceName];

         return strategy(resource, request.user as User);
    };

    if (securityName === Securities.CanUpdate) {
        return resolveWithUser(canProceed((resource, user) => authService.canUpdate(resource as UserOwnedResource, user)), 'User is not allowed to update resource');
    }

    if (securityName === Securities.CanDelete) {
        return resolveWithUser(canProceed((resource, user) => authService.canDelete(resource as UserOwnedResource, user)), 'User is not allowed to delete resource');
    }

    if (securityName === Securities.CanCreate) {
        let proceed = authService.canCreate(scopes[0] as Resources, request.user as User);
        return resolveWithUser(proceed, 'User is not allowed to create resource');
    }
}
