/* istanbul ignore file */

import * as express from 'express';
import { LevelsMiddleware } from '../Levels/LevelsMiddleware';
import { inject } from 'inversify';
import { RouteConfiguration } from './RouteConfiguration';
import { RouteSymbols } from './RouteSymbols';
import { provideSingleton } from '../Utils/provideSingleton';

@provideSingleton(RouteSymbols.RouteConfiguration, true)
export class LevelsRoutes implements RouteConfiguration {
    constructor(
        @inject(LevelsMiddleware) private levelsMiddleware: LevelsMiddleware,
    ) {
    }

    configureRoutes(app: express.Application) {
        // routes are now all bound by tsoa, this is only for middleware until it can be cleaned up more
        // todo

        // Finish by binding the Level middleware
        app.param('levelId', this.levelsMiddleware.levelByID);
    }
}
