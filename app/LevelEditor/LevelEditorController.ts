import { inject } from 'inversify';
import { LevelEditorService } from './LevelEditorService';
import { provide } from 'inversify-binding-decorators';
import { Get, Route } from 'tsoa';

@provide(LevelEditorController)
@Route('levelEditor')
export class LevelEditorController {
    constructor(
        @inject(LevelEditorService) private levelEditorService: LevelEditorService
    ) {
    }

    @Get('options')
    public async getLevelEditorOptions() {
        return this.levelEditorService.getOptions();
    }
}
