import { SizeOptions } from '../Size/SizeOptions';
import { inject } from 'inversify';
import { SizeFormatter } from '../Size/SizeFormatter';
import { provideSingleton } from '../Utils/provideSingleton';
import { LevelEditorSelectOptionsViewModel } from './LevelEditorSelectOptionsViewModel';


@provideSingleton(LevelEditorService)
export class LevelEditorService {
    constructor(
        @inject(SizeFormatter) private sizeFormatter: SizeFormatter
    ) {
    }

    public getOptions(): LevelEditorSelectOptionsViewModel {
        let sizeOptions: { [key: string]: number } = {};

        SizeOptions.Options.forEach(option => {
            // sizes in sizeOptions are per-side, but the editor needs to know the total number
            // this assumes square matrices, as most of the app does
            let key = this.sizeFormatter.formatSize(option);
            sizeOptions[key] = Math.pow(option, 2);
        });

        return { sizeOptions };
    }
}
