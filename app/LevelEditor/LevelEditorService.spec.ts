import { Container } from 'inversify';
import { SizeFormatter } from '../Size/SizeFormatter';
import { LevelEditorService } from './LevelEditorService';

describe('LevelEditorService', () => {
    let service: LevelEditorService;

    beforeEach(() => {
        let DIContainer = new Container();
        DIContainer.bind(SizeFormatter).toSelf();
        DIContainer.bind(LevelEditorService).toSelf();
        service = DIContainer.get(LevelEditorService);
    });

    it('should exist', () => {
        expect(service).toBeTruthy();
    });

    it('should return options correctly', () => {
        let options = service.getOptions();

        expect(options.sizeOptions).toEqual({ '5x5': 25, '10x10': 100, '15x15': 225 });
    });
});
