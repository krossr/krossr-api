export interface LevelEditorSelectOptionsViewModel {
    sizeOptions: { [key: string]: number };
}
