import { inject } from 'inversify';
import { ChangePasswordRequest } from './ChangePasswordRequest';
import { PasswordService } from '../Password/PasswordService';
import { UserService } from '../Users/UserService';
import { provide } from 'inversify-binding-decorators';
import { Body, Post, Request, Res, Route, Security, TsoaResponse } from 'tsoa';
import { ErrorResponse } from '../Error/ErrorResponse';
import { ErrorMessageService } from '../Error/ErrorMessageService';
import { Securities } from '../Authorization/Securities';
import { ChangePasswordBodyViewModel } from './ChangePasswordBodyViewModel';

@provide(ChangePasswordController)
@Route('users')
export class ChangePasswordController {
    constructor(
        @inject(ErrorMessageService) private errorMessageService: ErrorMessageService,
        @inject(PasswordService) private passwordService: PasswordService,
        @inject(UserService) private userService: UserService
    ) {
    }

    @Post('password')
    @Security(Securities.RequiresLogin)
    public async changePassword(
        @Request() req: ChangePasswordRequest,
        @Body() passwordDetails: ChangePasswordBodyViewModel,
        @Res() clientError: TsoaResponse<400, ErrorResponse>,
        @Res() serverError: TsoaResponse<500, ErrorResponse>
    ): Promise<void> {
        if (!passwordDetails.newPassword) {
            clientError(400, { message: 'Please provide a new password' });
        }

        if (passwordDetails.newPassword !== passwordDetails.verifyPassword) {
            clientError(400, { message: 'Passwords do not match' });
        }

        if (!req.user) {
            clientError(400, { message: 'User is not signed in' });
        }

        try {
            let user = await this.userService.getUserModel(req.user.id);

            if (!user) {
                clientError(400, { message: 'User is not found' });
            }

            if (!this.passwordService.authenticate(user, passwordDetails.currentPassword)) {
                clientError(400, { message: 'Current password is incorrect' });
            }

            this.passwordService.setPassword(user, passwordDetails.newPassword);

            await this.userService.saveUser(user);

            return new Promise((resolve, reject) => {
                req.login(user, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            });
        } catch (err) {
            serverError(500, this.errorMessageService.getErrorResponse(err));
        }
    }
}
