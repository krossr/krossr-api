import { Response } from 'express';
import { KrossrErrorBody } from './KrossrErrorBody';

export interface KrossrErrorResponse extends Response<KrossrErrorBody> {
}
