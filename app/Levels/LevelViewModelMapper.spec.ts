import { LevelViewModelMapper } from './LevelViewModelMapper';
import { Level } from '../models/LevelModel';
import { SequelizeContainer } from '../../spec/containers/sequelize-container';
import { User } from '../models/UserModel';
import { TestHelpers } from '../../spec/support/TestHelpers';

describe('LevelViewModelMapper', () => {
    let mapper: LevelViewModelMapper;

    beforeEach(async () => {
        let DIContainer = await new SequelizeContainer().initialize();
        DIContainer.bind(LevelViewModelMapper).toSelf();
        mapper = DIContainer.get(LevelViewModelMapper);
    });

    it('should exist', () => {
        expect(mapper).toBeTruthy();
    });

    it('should map correctly', () => {
        let result = { id: 1, layout: 'testLayout', name: 'test', size: 25, canBanLayout: false, canDelete: false };
        let user = User.build(TestHelpers.getTestUserAttributes());
        let model = Level.build(Object.assign(Object.assign({}, result), { userId: 1 }));
        expect(mapper.toViewModel(model, user)).toEqual(result);
    });
});
