import { Container } from 'inversify';
import { LevelsMiddleware } from './LevelsMiddleware';
import { ErrorHandler } from '../Error/ErrorHandler';
import { MockResponse } from '../../spec/support/MockResponse';
import { LevelRepository } from './LevelRepository';
import { User } from '../models/UserModel';
import { Roles } from '../Authorization/Roles';
import { UserOwnedResource } from '../models/UserOwnedResource';
import { SequelizeContainer } from '../../spec/containers/sequelize-container';
import { ErrorContainer } from '../../spec/containers/error-container';

describe('LevelsMiddleware', () => {
    let container: Container;
    let middleware: LevelsMiddleware;
    let errorHandler: ErrorHandler;
    let repo: LevelRepository;

    class MockLevelRequest {
        level: UserOwnedResource;
        user: User;
    }

    function getUser(id: number, name: string) {
        let user = User.build({
            username: name,
            email: '',
            role: Roles.User,
            hashedPassword: '',
            salt: '',
            provider: 'local'
        });

        user.id = id;

        return user;
    }

    beforeEach(async () => {
        container = await new SequelizeContainer().initialize();
        await new ErrorContainer().initialize(container);
        container.bind(LevelsMiddleware).toSelf();
        container.bind(LevelRepository).toSelf().inSingletonScope();
        middleware = container.get(LevelsMiddleware);
        errorHandler = container.get(ErrorHandler);
        repo = container.get(LevelRepository);
    });

    it('should exist', () => {
        expect(middleware).toBeTruthy();
    });

    it('should return a level without ratings if a user is not logged in', async () => {
        let request = new MockLevelRequest();
        let response = new MockResponse();

        spyOn(repo, 'getLevelById');
        let next = jasmine.createSpy();

        await middleware.levelByID(request as any, response as any, next, '1');
        expect(repo.getLevelById).toHaveBeenCalled();
        expect(next).toHaveBeenCalled();
    });

    it('should return a level with ratings if a user is logged in', async () => {
        let request = new MockLevelRequest();
        request.user = getUser(1, 'rosalyn');
        let response = new MockResponse();

        spyOn(repo, 'getLevelByIdIncludeUserRatings');
        let next = jasmine.createSpy();

        await middleware.levelByID(request as any, response as any, next, '1');
        expect(repo.getLevelByIdIncludeUserRatings).toHaveBeenCalled();
        expect(next).toHaveBeenCalled();
    });
});
