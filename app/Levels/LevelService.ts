import btoa from 'btoa';
import { inject } from 'inversify';
import { Level, LevelCreationAttributes } from '../models/LevelModel';
import { User } from '../models/UserModel';
import { LevelViewModelMapper } from './LevelViewModelMapper';
import { LevelRepository } from './LevelRepository';
import { BannedLayoutService } from '../BannedLayout/BannedLayoutService';
import { provide } from 'inversify-binding-decorators';
import { CreateLevelBodyViewModel } from './CreateLevelBodyViewModel';
import { LevelViewModel } from './LevelViewModel';
import { UpdateLevelBodyViewModel } from './UpdateLevelBodyViewModel';

@provide(LevelService)
export class LevelService {
    constructor(
        @inject(BannedLayoutService) private bannedLayoutService: BannedLayoutService,
        @inject(LevelViewModelMapper) private levelMapper: LevelViewModelMapper,
        @inject(LevelRepository) private levelRepository: LevelRepository
    ) {
    }

    createLevelModel(attributes: LevelCreationAttributes) {
        return Level.build(attributes);
    }

    async createLevel(user: User, params: CreateLevelBodyViewModel): Promise<LevelViewModel> {
        let layout = this.encodeLayout(params.decodedLayout);

        if (await this.bannedLayoutService.isBanned(layout)) {
            return Promise.reject({ message: await this.bannedLayoutService.getMessageForLayout(layout) });
        }

        let attributes: LevelCreationAttributes = {
            name: params.name,
            layout: this.encodeLayout(params.decodedLayout),
            size: params.decodedLayout.length,
            userId: user.id
        };

        let level = await this.levelRepository.createLevel(attributes);

        return Promise.resolve(this.levelMapper.toViewModel(level, user));
    }

    async getLevel(user: User, level: Level): Promise<LevelViewModel> {
        let result = this.levelMapper.toViewModel(level, user);
        result.userRating = this.getUserRating(user, level);
        return Promise.resolve(result);
    }

    async getLevelById(user: User, id: string): Promise<LevelViewModel> {
        let level = await this.levelRepository.getLevelById(id);
        return await this.getLevel(user, level);
    }

    async deleteLevel(level: Level) {
        await this.levelRepository.deleteLevel(level);
    }

    async updateLevel(levelId: string, user: User, params: UpdateLevelBodyViewModel): Promise<LevelViewModel> {
        let level = await this.levelRepository.getLevelById(levelId);

        let update = {
            name: params.name,
            layout: this.encodeLayout(params.decodedLayout),
            size: params.decodedLayout.length
        };

        let result = await this.levelRepository.updateLevel(level, update);

        return Promise.resolve(this.levelMapper.toViewModel(result, user));
    }

    private encodeLayout(layout: boolean[][]) {
        let converted = Array.prototype.concat.apply([], layout) // flatten
            .map((value: boolean) => value ? '1' : '0')
            .join('');

        return btoa(converted);
    }

    private getUserRating(user: User, level: Level) {
        if (level.ratings && user) {
            return level.ratings.find(rating => rating.userId === user.id);
        }
    }
}
