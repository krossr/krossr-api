/* istanbul ignore file */
/** we trust that sequelize, or whatever database adapter is being used, knows what it is doing... */

import { LevelCreationAttributes, Level } from '../models/LevelModel';
import { IncludeOptions } from 'sequelize';
import { provide } from 'inversify-binding-decorators';

@provide(LevelRepository)
export class LevelRepository {
    async createLevel(attributes: LevelCreationAttributes) {
        return await Level.create(attributes);
    }

    async deleteLevel(level: Level) {
        return await level.destroy();
    }

    async updateLevel(level: Level, update: any) { // todo
        return await level.update(update);
    }

    async getLevelById(id: string) {
        return Level.findOne({ where: { id }});
    }

    async getLevelByIdIncludeUserRatings(id: string, userId: number) {
        let include =                 {
            association: Level.associations.ratings,
            attributes: ['rating', 'userId'],
            required: false,
            where: {
                userId
            }
        } as IncludeOptions;

        return Level.findOne({
            include,
            where: {
                id
            }
        });
    }
}
