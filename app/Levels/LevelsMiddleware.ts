import { inject } from 'inversify';
import { LevelRequest } from './LevelRequest';
import { Response } from 'express';
import { LevelRepository } from './LevelRepository';
import { provide } from 'inversify-binding-decorators';

@provide(LevelsMiddleware)
export class LevelsMiddleware {
    constructor(
        @inject(LevelRepository) private levelRepository: LevelRepository
    ) {
    }

    public levelByID = async (req: LevelRequest<void>, res: Response, next: (error?: Error) => void, id: string) => {
        let user = req.user;

        let promise = user ? this.levelRepository.getLevelByIdIncludeUserRatings(id, user.id)
                           : this.levelRepository.getLevelById(id);

        try {
            let level = await promise;
            req.level = level;
            return next();
        } catch (err) {
            return next(new Error('Failed to load level ' + id));
        }
    }
}
