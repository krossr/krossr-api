import { Container } from 'inversify';
import { ErrorContainer } from '../../spec/containers/error-container';
import { AuthorizationService } from '../Authorization/AuthorizationService';
import { BannedLayoutMiddleware } from './BannedLayoutMiddleware';

describe('BannedLayoutMiddleware', () => {
    let container: Container;
    let middleware: BannedLayoutMiddleware;

    beforeEach(async () => {
        container = new Container();
        await new ErrorContainer().initialize(container);
        container.bind(AuthorizationService).toSelf();
        container.bind(BannedLayoutMiddleware).toSelf();
        middleware = container.get(BannedLayoutMiddleware);
    });

    it('should exist', () => {
        expect(middleware).toBeTruthy();
    });
});
