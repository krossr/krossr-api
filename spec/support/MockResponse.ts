export class MockResponse {
    private res = {};

    status(code: number) {
        return {
            send: () => {
                return this.res;
            }
        };
    }

    json = () => this.res;
    jsonp = (data: any) => this.res;
    send = () => this.res;
}
