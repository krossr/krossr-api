import { UserCreationAttributes } from '../../app/models/UserModel';
import { LevelCreationAttributes } from '../../app/models/LevelModel';
import { Roles } from '../../app/Authorization/Roles';

export class TestHelpers {
    static getTestUserAttributes(): UserCreationAttributes {
        return {
            email: 'fakeemail@fake.com',
            username: 'rosalyn',
            provider: '',
            salt: '',
            hashedPassword: '',
            role: Roles.User
        };
    }

    static getTestLevelAttributes(userId: number): LevelCreationAttributes {
        return {
            name: 'wat',
            userId,
            size: 25,
            layout: 'MTAwMDExMDAwMTEwMDAxMTAwMDExMTExMQ'
        };
    }
}
