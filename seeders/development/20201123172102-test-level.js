'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('levels', [{
     id: 1,
     name: 'Seed Test',
     layout: 'MTExMTExMDAwMTEwMDAxMTAwMDExMTExMQ==',
     size: 5,
     userId: 1,
     createdAt: new Date(),
     updatedAt: new Date()
   }]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('levels', null, {});
  }
};
