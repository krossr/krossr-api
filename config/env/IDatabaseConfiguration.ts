import { SequelizeConfiguration } from '../database/SequelizeConfiguration';

export interface IDatabaseConfiguration extends SequelizeConfiguration {
    port: number;
}
