import { provide } from 'inversify-binding-decorators';

@provide(EnvironmentConfigurationDefaults)

export class EnvironmentConfigurationDefaults {
    private baseName = process.env.BASE_NAME ?? 'krossr';

    getDefaults() {
        return {
            app: {
                title: 'krossr',
                description: 'Full-Stack JavaScript with MongoDB, Express, AngularJS, and Node.js',
                keywords: 'MongoDB, Express, AngularJS, Node.js'
            },
            baseName: this.baseName,
            port: process.env.PORT || 3000,
            minPasswordLength: 10,
            production: false,
            templateEngine: 'swig',
            sessionSecret: 'MEAN',
            sessionCollection: 'sessions',
            mailer: {
                from: `support@${this.baseName}.com`
            }
        };
    }
}
