import { Dialect } from 'sequelize';

export interface SequelizeConfiguration {
    username: string;
    password: string;
    database: string;
    host: string;
    dialect: Dialect;
}
