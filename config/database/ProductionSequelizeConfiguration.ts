import { SequelizeConfiguration } from './SequelizeConfiguration';

export class ProductionSequelizeConfiguration implements SequelizeConfiguration {
    dialect: 'postgres' = 'postgres';
    database = 'krossr';
    username = 'krossr';
    password = process.env.PG_PWD;
    host = process.env.DB_HOST;
    port = 5432;
}
