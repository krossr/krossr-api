import * as _ from 'lodash';
import { EnvironmentConfigurationDefaults } from './env/all';
import { DevelopmentEnvironmentConfiguration } from './env/development';
import { IEnvironmentConfiguration } from './env/IEnvironmentConfiguration';
import { inject } from 'inversify';
import { TestEnvironmentConfiguration } from './env/test';
import { ProductionEnvironmentConfiguration } from './env/production';
import { provideSingleton } from '../app/Utils/provideSingleton';

/**
 * Load app configurations
 */
@provideSingleton(EnvironmentConfiguration)
export class EnvironmentConfiguration {
    constructor(
        @inject(EnvironmentConfigurationDefaults) private environmentConfigurationDefaults: EnvironmentConfigurationDefaults
    ) {
    }

    getConfiguration() {
        return _.extend(
            this.environmentConfigurationDefaults.getDefaults(),
            this.getConfig()
        ) as IEnvironmentConfiguration;
    }

    private getConfig() {
        switch (process.env.NODE_ENV) {
            case 'development':
                return new DevelopmentEnvironmentConfiguration();

            case 'test':
                return new TestEnvironmentConfiguration();

            case 'production':
                return new ProductionEnvironmentConfiguration();

            default:
                throw new Error(`process.env.NODE_ENV=${process.env.NODE_ENV} is not a supported environment!`);
        }
    }
}
