// this file should ONLY be imported into server.ts

import { Container, decorate, injectable } from 'inversify';
import { ModelConfiguration } from './app/models/ModelConfiguration';
import { Sequelize } from 'sequelize';
import { ModelSymbols } from './app/models/ModelSymbols';
import { LevelConfiguration } from './app/models/LevelModel';
import { RatingConfiguration } from './app/models/RatingModel';
import { UserConfiguration } from './app/models/UserModel';
import { CompletedLevelConfiguration } from './app/models/CompletedLevelModel';
import { BannedLayoutConfiguration } from './app/models/BannedLayoutModel';
import { buildProviderModule } from 'inversify-binding-decorators';
import { Controller } from 'tsoa';

// todo shouldn't have to explicitly import everything
import './app/Authorization/AuthorizationService';
import './app/Error/ErrorHandler';
import './app/Error/ErrorMessageService';
import './app/HtmlRenderer/HtmlRenderer';
import './app/LevelList/LevelListLevelViewModelMapper';
import './app/Levels/LevelViewModelMapper';
import './app/LevelList/LevelListService';
import './app/LevelList/LevelListController';
import './app/Ratings/RatingsController';
import './app/Levels/LevelsMiddleware';
import './app/Routes/LevelsRoutes';
import './app/Levels/LevelsController';
import './app/Users/UserViewModelMapper';
import './app/Mailer/MailerService';
import './app/Users/SignOutController';
import './app/Users/UserProfileController';
import './app/SignIn/SignInController';
import './app/SignUp/SignUpController';
import './app/ChangePassword/ChangePasswordController';
import './app/ForgotPassword/ForgotPasswordController';
import './app/ResetPassword/ResetPasswordController';
import './config/expressConfig';
import './app/Routes/RouteConfiguration';
import './app/Routes/RouteSymbols';
import './app/Levels/LevelService';
import './config/sequelizeConfig';
import './config/strategies/AuthenticationStrategy';
import './config/strategies/AuthenticationStrategySymbols';
import './config/strategies/LocalPassportStrategy';
import './config/passportConfig';
import './config/environmentConfig';
import './config/env/all';
import './app/Logger/KrossrLoggerProvider';
import './app/Logger/LoggerSymbols';
import './config/winston';
import './app/Ratings/RatingsService';
import './app/Password/PasswordService';
import './app/SignUp/SignUpService';
import './app/Size/SizeFormatter';
import './app/LevelEditor/LevelEditorService';
import './app/LevelEditor/LevelEditorController';
import './app/Mailer/IMailerService';
import './app/Mailer/MailerSymbols';
import './app/Authentication/AuthenticationService';
import './app/Levels/LevelRepository';
import './app/Ratings/RatingsRepository';
import './app/LevelList/LevelListRepository';
import './app/Crypto/CryptoService';
import './app/Users/UserService';
import './app/Users/UserRepository';
import './app/CompletedLevel/CompletedLevelController';
import './app/CompletedLevel/CompletedLevelService';
import './app/CompletedLevel/CompletedLevelRepository';
import './app/Authorization/AuthorizationService';
import './app/BannedLayout/BannedLayoutMiddleware';
import './app/BannedLayout/BannedLayoutService';
import './app/BannedLayout/BannedLayoutRepository';
import './app/BannedLayout/BannedLayoutController';

let iocContainer = new Container();

decorate(injectable(), Controller);

iocContainer.load(buildProviderModule());

/* Models -- Order important! */
iocContainer.bind<ModelConfiguration<Sequelize>>(ModelSymbols.ModelConfiguration).to(RatingConfiguration);
iocContainer.bind<ModelConfiguration<Sequelize>>(ModelSymbols.ModelConfiguration).to(UserConfiguration);
iocContainer.bind<ModelConfiguration<Sequelize>>(ModelSymbols.ModelConfiguration).to(LevelConfiguration);
iocContainer.bind<ModelConfiguration<Sequelize>>(ModelSymbols.ModelConfiguration).to(CompletedLevelConfiguration);
iocContainer.bind<ModelConfiguration<Sequelize>>(ModelSymbols.ModelConfiguration).to(BannedLayoutConfiguration);

export { iocContainer };
