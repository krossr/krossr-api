const Sequelize = require('sequelize');

module.exports = {
  up: async (queryInterface) => {
    let tableName = 'users';

    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    queryInterface.describeTable(tableName)
      .then(async (def) => {
        if (def.role) {
          await Promise.resolve();
        } else {
          await queryInterface.addColumn(
            'users', // TableNames.Users TODO
            'role',
            Sequelize.DataTypes.STRING
          );
        }
      });
  },

  down: async (queryInterface) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeColumn(
      'users',
      'role'
    );
  }
};
